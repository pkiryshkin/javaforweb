import java.util.Scanner;
public class SwitchCase {
	static String SwitchChanger(String str){
		char ch  = str.charAt(0);
		String str_1 = Character.toString(ch);

		if (str_1.equals(str_1.toUpperCase())){
			return str_1.toLowerCase() + str.substring(1);
		} else {
			return str_1.toUpperCase() + str.substring(1);
		}
		
		
	}
	
	public static void main(String[] args) {
		Scanner inp = new Scanner( System.in );
		
		System.out.println("Enter string");
		String str = inp.next();
		str = SwitchChanger(str);
		System.out.print(str);
	}

}