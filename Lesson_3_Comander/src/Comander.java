
import java.io.*;
class Comander 
{
 public static void main(String args[])
 
  {
	String str1=args[0];
	
	if (str1.equals("create")){
		createfile(args[1]);
	}else if (str1.equals("delete")){
		deletefile(args[1]);
	}else if (str1.equals("rename")){
		renamefile(args[1], args[2]);
	}
}
  
  private static void createfile(String newDir){
	try{
			
		// Create one directory
		File dir = new File (newDir);
		if ( dir.exists()) {  
			System.out.println("Folder, are exist");
			System.exit(0);  
		}
		boolean success = dir.mkdir();
		if (success) {
			System.out.println("Directory: "    + dir + " created");
		}  
  
	}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
	}
 }
 private static void renamefile(String oldDir, String newDir){

	File dir = new File(oldDir);  
    File newName = new File(newDir);  
    if ( dir.exists()) {  
        System.out.println("Folder are rename");
        dir.renameTo(newName);  
    } else {
        System.out.println("Folder does not exist");
        System.exit(0);
    }  
}
 private static void deletefile(String dir){
	File f1 = new File(dir);
	if ( !f1.exists()) {
		System.out.print("Folder does not exist");
		System.exit(0);
	}
	boolean success = f1.delete();
	if (!success){
		System.out.println("Deletion failed.");
		System.exit(0);
	}else{
		System.out.println("Folder deleted.");
    }
  }
}