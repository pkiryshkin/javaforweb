import java.io.*;
import java.net.*;
public class Client
{
  public static final int DEFAULT_PORT = 6789;
  
  // usage------------------------------------------------------
  // specifies correct usage and exits if host name missing
  
  protected static void usage()
  {
    System.out.println("Usage: java Client <hostname> [<port>]");
    System.exit(1);
  }
    
  // main ------------------------------------------------------
  // All the work is done here
  
  public static void main(String args[])
  {
    int port = DEFAULT_PORT;
    Socket s = null;
    
     
     try
     {
     	// create a socket to communicate with specified host and port
     	s = new Socket("127.0.0.1",14);
     	
     	// create streams for reading and writing lines of text to this socket
        PrintWriter out = new PrintWriter(s.getOutputStream());     	
     	InputStreamReader in = new InputStreamReader(s.getInputStream());
     	BufferedReader bufReader = new BufferedReader(in);
     	
     	
       	// create a reader to read lines of text from keyboard
     	InputStreamReader kin = new InputStreamReader(System.in);
     	BufferedReader kbuf = new BufferedReader(kin);
     	 
     	// tell user we are connected
     	System.out.println("Connected to " + s.getInetAddress() + " on port " + s.getPort());
     	
     	String line = null;
     	
     	while (true)
     	{
     	  // prompt user
     	  System.out.println("Enter a line: ");
     	  line = kbuf.readLine();
     	  if (line == null)
     	    break;
     	  
     	  //send it to the server
     	  out.println(line);
     	  out.flush();
     	  
     	  //read server response
     	  line = bufReader.readLine();
     	  
     	  //check if connection is closed (i.e., EOF)
     	  if (line == null)
     	  {
     	    System.out.println("Connection closed by server");
     	    break;
     	  }
     	  
     	  //write response to console
     	  System.out.println(line);
     	} // end while
     } // end try
     catch (IOException e) {System.err.println(e);}
     
     //always be sure to close the socket
     finally
     {
       try
       {
       	 if (s != null)
       	   s.close();
       }
       catch (IOException e2) {}
     }
  } // end main
} // end client