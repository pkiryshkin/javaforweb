import java.io.*;
import java.util.*;
import java.net.*;

public class Server {
	
	protected static final int PORT_NUMBER = 14;
	
	public static void main( String args[]) {
				
		try {
		
			ServerSocket servsock = new ServerSocket(PORT_NUMBER);
						
			String instr;
		
			while(true) {
			
				Socket sock = servsock.accept();
				Scanner in = new Scanner(sock.getInputStream());
				PrintWriter out = new PrintWriter(sock.getOutputStream());
												
				System.out.println(in.next().charAt(0));
				
				in.close();
				out.close();
				sock.close();
				servsock.close();
			}
			
			
		} catch(Exception e) {
			System.out.println(e.toString());
		}
		
	}
	
}