import java.io.*;
import java.util.*;
import java.net.*;

public class Client {
	
	protected static Socket sock;
	
	public static void main(String args[]) {
		try {
			sock = new Socket("127.0.0.1",14);
			DataInputStream response = new DataInputStream(sock.getInputStream());
			PrintWriter request = new PrintWriter(sock.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String txt = "";
						
			System.out.print("Enter string: ");
			txt = in.readLine();
			request.print(txt);
			request.flush();
			
			response.close();
			request.close();
			in.close();
			sock.close();

			
		} catch(IOException e) {
			System.out.println(e.toString());
		}
	}
	
}