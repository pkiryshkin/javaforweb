import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

import com.mysql.jdbc.Statement;
//import com.mysql.jdbc.PreparedStatement;


public class Main {
        public static void main(String[] args) throws Exception{
                //Accessing driver from the JAR file
                Class.forName("com.mysql.jdbc.Driver");
                
                //Creating a variable for the connection called "con"
                Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/testdb","root","");
                /*
                PreparedStatement statement = con.prepareStatement("select * from names");
                
                ResultSet result = statement.executeQuery();
                
                /*while(result.next()){
                        System.out.println(result.getString(1) + " " + result.getString(2));
                }*/
                
                System.out.println("Insert element in table and view table.");
                Statement insert = (Statement) con.createStatement();
                insert.executeUpdate("INSERT INTO   `testdb`.`names` (`first` ,`last`) " + "VALUES ('d',  'c')");
                
                PreparedStatement view_table_1 = con.prepareStatement("select * from names");
                
                ResultSet result_1 = view_table_1.executeQuery();
                while(result_1.next()){
                        System.out.println(result_1.getString(1) + " " + result_1.getString(2));
                }
                
                System.out.println("Delete element from table and view.");
                Statement delete = (Statement) con.createStatement();
                delete.executeUpdate("DELETE FROM `testdb`.`names` WHERE first='d'");
                
                PreparedStatement view_table_2 = con.prepareStatement("select * from names");
                
                ResultSet result_2 = view_table_2.executeQuery();
                while(result_2.next()){
                        System.out.println(result_2.getString(1) + " " + result_2.getString(2));
                }

        }
}