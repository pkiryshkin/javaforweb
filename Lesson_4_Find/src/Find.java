import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import static java.nio.file.FileVisitResult.*;
import static java.nio.file.FileVisitOption.*;
import java.util.*;
 

public class Find extends Thread{
 
    public static class Finder  extends SimpleFileVisitor<Path> {
 
        private final PathMatcher matcher;
        private int numMatches = 0;
 
        Finder(String pattern) {
            matcher =  FileSystems.getDefault().getPathMatcher("glob:" + pattern);
        }
       
       void find(Path file) {
            Path name = file.getFileName();
            if (name != null && matcher.matches(name)) {
                numMatches++;
                System.out.println(file);
            }
        }
 
        void done() {
            System.out.println("Matched: "+ numMatches);
        }
 
       public FileVisitResult     visitFile(Path file, BasicFileAttributes attrs) {
            find(file);
            return CONTINUE;
        }
 
 }
  
    public static void main(String[] args)
        throws IOException {
        (new Find()).start();
        Path startingDir = Paths.get("D:\\geekhub");
        String pattern = "*.java";
 
        Finder finder = new Finder(pattern);
        Files.walkFileTree(startingDir, finder);
        finder.done();
        
    }
}