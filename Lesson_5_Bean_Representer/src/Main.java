public class Main {
        public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
                Cat cat = new Cat("Tom");
                
                cat.setColor("black");
                cat.setLegCount(4);
                
                Human human = new Human("Joe");
                
                human.setage(22);
                human.setsex("Men");
                
                Car car = new Car("Audi");
                
                car.setColor("Black");
                car.setspeed(250);
                
                Representer CatRep = new Representer();
                CatRep.present(cat);
                
                Representer CarRep = new Representer();
                CarRep.present(car);
                
                Representer HumRep = new Representer();
                HumRep.present(human);
                
                
        }
}