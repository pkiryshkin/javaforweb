public class Car {
        
        public Car(String model) {
                super();
                this.model = model;
        }
        
        private String model;
        private String color;
        private int speed;
        
        public String getmodel() {
                return model;
        }
        public void setmodel(String model) {
                this.model = model;
        }
        public String getColor() {
                return color;
        }
        public void setColor(String color) {
                this.color = color;
        }
        public int getspeed() {
                return speed;
        }
        public void setspeed(int speed) {
                this.speed = speed;
        }
        
        
}