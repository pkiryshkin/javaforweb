import java.lang.reflect.Field;


public class Representer {
        public void present(Object o1) throws IllegalArgumentException, IllegalAccessException {
                Class clazz = o1.getClass();
                Field[] fields = clazz.getDeclaredFields();
                boolean equals = true;
                for (Field field : fields) {
                        field.setAccessible(true);
                        Object value1 = field.get(o1);
                        System.out.println(field.getName() + "          " + value1);
                }
                System.out.println();
        }
}