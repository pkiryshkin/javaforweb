public class Human {
        
        public Human(String name) {
                super();
                this.name = name;
        }
        
        private String name;
        private int age;
        private String sex;
        
        public String getname() {
                return name;
        }
        public void setname(String name) {
                this.name = name;
        }
        public int getage() {
                return age;
        }
        public void setage(int age) {
                this.age = age;
        }
        public String getsex() {
                return sex;
        }
        public void setsex(String sex) {
                this.sex = sex;
        }
        
        
}