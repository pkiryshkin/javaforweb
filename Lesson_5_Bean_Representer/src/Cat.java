public class Cat {
        
        public Cat(String name) {
                super();
                this.name = name;
        }
        

        private String name;
        private String color;
        private int legCount;
        
        public String getName() {
                return name;
        }
        public void setName(String name) {
                this.name = name;
        }
        public String getColor() {
                return color;
        }
        public void setColor(String color) {
                this.color = color;
        }
        public int getLegCount() {
                return legCount;
        }
        public void setLegCount(int legCount) {
                this.legCount = legCount;
        }
        
        
}